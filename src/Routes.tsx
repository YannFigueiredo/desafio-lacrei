import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from './pages/Home';
import User from './pages/User';
import Profissional from './pages/Profissional';
import Header from "./components/Header";
import Footer from "./components/Footer";

export default function RoutesApp() {
    return(
        <BrowserRouter>
            <Header />
            <Routes>
                <Route path="/" element={<Home />}/>
                <Route path="/user" element={<User />}/>
                <Route path="/profissional" element={<Profissional />}/>
            </Routes>
            <Footer />
        </BrowserRouter>
    );
}