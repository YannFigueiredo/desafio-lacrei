export const theme = {
    colors: {
        primary: '#FFFFFF',
        secondary: '#EEEEEE',
        green: '#018762',
        green_hover: 'rgb(78, 171, 145)',
        light_green: '#B0E0D3',
        primary_font: '#1F1F1F',
        secondary_font: '#515151' 
    },
    fonts_size: [16, 18, 24, 32, 48, 14],
    spacing: {
        large: 64,
        mid: 48,
        small: 32,
        big_small: 28,
        very_small: 16
    }
}

//Incluir fonte 14px