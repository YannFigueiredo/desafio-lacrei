import { ThemeProvider } from 'styled-components';
import { theme } from '../theme/Theme';
import { GlobalStyles } from '../GlobalStyles';

export const AppProviders = ({ children }: { children: React.ReactNode }) => {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      {children}
    </ThemeProvider>
  )
}