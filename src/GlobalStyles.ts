import { createGlobalStyle } from "styled-components";
import { theme } from './theme/Theme';

export const GlobalStyles = createGlobalStyle `
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Nunito', 'sans-serif';
    }

    h1 {
        color: ${theme.colors.primary_font};
        font-weight: 700;
        font-size: ${theme.fonts_size[4]}px;
    }

    p {
        color: ${theme.colors.secondary_font};
        font-weight: 400;
        font-size: ${theme.fonts_size[2]}px;
    }

    span {
        color: ${theme.colors.secondary_font};
        font-weight: 400;
    }

    img {
        max-width: 100%;
        width: 100%;
    }

    @media screen and (max-width: 1040px) {
        h1 {
            font-size: ${theme.fonts_size[3]}px;
        }

        p {
            font-size: ${theme.fonts_size[0]}px;
        }
    }

    @media screen and (max-width: 520px) {
        h1 {
            font-size: ${theme.fonts_size[2]}px;
        }

        p {
            font-size: ${theme.fonts_size[5]}px;
        }
    }
`