import styled from "styled-components";

export const ContainerLeft = styled.div `
    width: 570px;

    h1 {
        display: inline-block;
        margin-bottom: ${({ theme }) => theme.spacing.small}px;
    }

    p {
        display: inline-block;
        padding-left: 24px;
        border-left: 5px solid ${({ theme }) => theme.colors.green};
    }

    @media screen and (max-width: 1040px) {
        width: auto;
        max-width: 540px;
        margin: auto;
    }

    @media screen and (max-width: 520px) {
        h1 {
            margin-bottom: ${({ theme }) => theme.spacing.very_small}px;
        }
    }
`

export const ContainerRight = styled.div `
    width: 540px;
    height: 421px;

    @media screen and (max-width: 520px) {
        width: auto;
        margin-top: ${({ theme }) => theme.spacing.very_small}px;
    }
`