import { Container } from "../styles";
import { ContainerLeft, ContainerRight } from "./styles";
import pessoaUsuaria from "../../assets/img/pessoaUsuariaImg.svg";

export default function User() {
    return(
        <Container>
            <ContainerLeft>
                <h1>Pessoa Usuária</h1>
                <p>
                    A Lacrei garante que pessoas LGBTQIAPN + recebam atendimento realizado por profissionais de qualidade e que atendam às suas necessidades de forma segura e acolhedora.
                </p>
            </ContainerLeft>
            <ContainerRight>
                <img src={pessoaUsuaria} alt="Imagem principal da página de usuário"/>
            </ContainerRight>
        </Container>
    );
}