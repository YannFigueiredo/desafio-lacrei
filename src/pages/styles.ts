import styled from 'styled-components';

export const Container = styled.div `
    padding: ${({ theme }) => theme.spacing.large}px ${({ theme }) => theme.spacing.large}px 0px ${({ theme }) => theme.spacing.large}px;
    display: flex;
    justify-content: space-between;

    @media screen and (max-width: 1040px) {
        flex-direction: column;
        justify-content: center;
        align-items: center;
        padding: ${({ theme }) => theme.spacing.small}px ${({ theme }) => theme.spacing.small}px 0px ${({ theme }) => theme.spacing.small}px;
    }

    @media screen and (max-width: 520px) {
        padding: ${({ theme }) => theme.spacing.very_small}px ${({ theme }) => theme.spacing.very_small}px 0px ${({ theme }) => theme.spacing.very_small}px;
    }
`