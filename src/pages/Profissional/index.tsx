import { Container } from "../styles";
import { ContainerLeft, ContainerRight } from "./styles";
import profissional from "../../assets/img/profissional.svg";

export default function Profissional() {
    return(
        <Container>
            <ContainerLeft>
                <h1>Profissional</h1>
                <p>
                    Buscamos recrutar pessoas profissionais da saúde que entendam as necessidades e se comprometam com o bem-estar da comunidade LGBTQIAPN+
                </p>
            </ContainerLeft>
            <ContainerRight>
                <img src={profissional} alt="Imagem principal da página de profissional"/>
            </ContainerRight>
        </Container>
    );
}