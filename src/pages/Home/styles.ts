import styled from "styled-components";

export const ContainerLeft = styled.div `
    width: 550px;

    h1 {
        display: inline-block;
        margin-bottom: ${({ theme }) => theme.spacing.small}px;
    }

    p {
        display: inline-block;
        margin-bottom: ${({ theme }) => theme.spacing.mid}px;
    }

    @media screen and (max-width: 1040px) {
        width: auto;
        max-width: 540px;
        margin: auto;

        h1, p {
            width: 100%;
        }
    }

    @media screen and (max-width: 520px) {
        h1 {
            margin-bottom: ${({ theme }) => theme.spacing.very_small}px;
        }
    }
`

export const ContainerRight = styled.div `
    width: 555px;
    height: 421px;

    @media screen and (max-width: 520px) {
        width: auto;
        margin-top: ${({ theme }) => theme.spacing.small}px;
    }
`

export const Buttons = styled.div `
    display: flex;
    justify-content: space-between;
    align-items: center;

    
    @media screen and (max-width: 1040px) {
        width: auto;
        max-width: 470px;
    }

    @media screen and (max-width: 520px) {
        flex-direction: column;
        height: 110px;
    }
`