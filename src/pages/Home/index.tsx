import { Container } from "../styles";
import { ContainerLeft, ContainerRight, Buttons } from "./styles";
import Button from "../../components/Button";
import home from '../../assets/img/home.svg';

export default function Home() {
    return(
        <Container>
            <ContainerLeft>
                <h1>Boas vindas a Lacrei Saúde</h1>
                <p>Uma plataforma segura e acolhedora para comunidade LGBTQIAPN+</p>
                <Buttons>
                    <Button text="Pessoa Usuária" href="/user" model="green"/>
                    <Button text="Profissional" href="/profissional" model="white"/>
                </Buttons>
            </ContainerLeft>
            <ContainerRight>
                <img src={home} alt="Imagem principal"/>
            </ContainerRight>
        </Container>
    );
}