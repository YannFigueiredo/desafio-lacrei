import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { AppProviders } from "../../providers/AppProviders";
import User from "../User"; 

describe('User tests', () => {
    test('should render correctly', () => {
        render(
            <User />, 
            {wrapper: AppProviders}
        );

        expect(screen.getByText("Pessoa Usuária")).toBeInTheDocument();
        expect(screen.getByAltText("Imagem principal da página de usuário")).toBeInTheDocument();
    });
});
