import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { AppProviders } from "../../providers/AppProviders";
import Home from "../Home"; 

describe('Home tests', () => {
    test('should render correctly', () => {
        render(
            <Home />, 
            {wrapper: AppProviders}
        );

        expect(screen.getByText("Boas vindas a Lacrei Saúde")).toBeInTheDocument();
        expect(screen.getByText("Pessoa Usuária")).toBeInTheDocument();
        expect(screen.getByAltText("Imagem principal")).toBeInTheDocument();
    });
});
