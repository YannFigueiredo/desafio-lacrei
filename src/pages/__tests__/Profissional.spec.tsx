import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { AppProviders } from "../../providers/AppProviders";
import Profissional from "../Profissional"; 

describe('Profissional tests', () => {
    test('should render correctly', () => {
        render(
            <Profissional />, 
            {wrapper: AppProviders}
        );
        
        expect(screen.getByText("Profissional")).toBeInTheDocument();
        expect(screen.getByAltText("Imagem principal da página de profissional")).toBeInTheDocument();
    });
});
