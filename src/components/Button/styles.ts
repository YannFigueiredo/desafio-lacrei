import styled from "styled-components";

interface ButtonProps {
    model: string;
}

export const Container = styled.a<ButtonProps> `
    display: flex;
    justify-content: center;
    align-items: center;
    text-decoration: none;
    width: 192px;
    height: 48px;
    border-radius: 8px;
    color: ${({ model, theme }) => model === 'green' ? theme.colors.primary : theme.colors.green};
    background: ${({ model, theme }) => model === 'green' ? theme.colors.green : theme.colors.primary};
    border: 2px solid ${({ model, theme }) => model === 'green' ? 'none' : theme.colors.green};
    transition: all ease .2s;
    box-shadow: rgb(0 0 0 / 25%) 0px 4px 4px;
    font-weight: 700;
    font-size: ${({ theme }) => theme.fonts_size[1]}px;

    &:hover {
        color: ${({ model, theme }) => model === 'green' ? theme.colors.primary : theme.colors.green_hover};
        background: ${({ model, theme }) => model === 'green' ? theme.colors.green_hover : theme.colors.primary};
        border: 2px solid ${({ model, theme }) => model === 'green' ? 'none' : theme.colors.green_hover};
    }
`