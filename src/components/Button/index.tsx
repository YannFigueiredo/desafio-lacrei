import { Container } from './styles';

type Props = {
    text: string,
    model: string,
    href: string
}

export default function Button({ text, model, href }: Props) {
    return(
        <Container role="button" href={href} model={model}>
            { text }
        </Container>
    );
}