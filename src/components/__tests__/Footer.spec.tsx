import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import Footer from "../Footer";
import { AppProviders } from "../../providers/AppProviders"; 

describe('Footer tests', () => {
    test('should render correctly', () => {
        render(
            <Footer />, 
            {wrapper: AppProviders}
        );

        expect(screen.getByText("Home")).toBeInTheDocument();
        expect(screen.getByText("Pessoa Usuária")).toBeInTheDocument();
        expect(screen.getByText("Profissional")).toBeInTheDocument();
        expect(screen.getByText("Desafio Front-end Lacrei")).toBeInTheDocument();
    });
});
