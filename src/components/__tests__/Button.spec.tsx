import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { AppProviders } from "../../providers/AppProviders"; 
import { theme } from "../../theme/Theme";
import Button from "../Button";

describe('Button tests', () => {
    test('should render correctly', () => {
        const text = "Clicar";
        const model = "green";

        render(
            <Button text={text} href="" model={model} />,
            {wrapper: AppProviders}
        );

        const buttonElement = screen.getByRole('button');

        expect(buttonElement).toHaveTextContent('Clicar');
    });

    test('should render with the right color according to the green model', () => {
        const model = "green";

        render(
            <Button text="" href="" model={model} />,
            {wrapper: AppProviders}
        );

        const buttonElement = screen.getByRole('button');

        expect(buttonElement).toHaveStyle({ color: theme.colors.primary, background: theme.colors.green, border: "2px none" });
    }); 

    test('should render with the right color according to another model', () => {
        const model = "white";

        render(
            <Button text="" href="" model={model} />,
            {wrapper: AppProviders}
        );

        const buttonElement = screen.getByRole('button');

        expect(buttonElement).toHaveStyle({ color: theme.colors.green, background: theme.colors.primary, border: "2px solid " + theme.colors.green });
    }); 
});

