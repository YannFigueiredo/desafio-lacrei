import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import Header from "../Header";
import { AppProviders } from "../../providers/AppProviders"; 

describe('Header tests', () => {
    test('should render correctly', () => {
        render(
            <Header />, 
            {wrapper: AppProviders}
        );

        expect(screen.getByText("Lacrei")).toBeInTheDocument();
        expect(screen.getByText("Home")).toBeInTheDocument();
        expect(screen.getByText("Pessoa Usuária")).toBeInTheDocument();
        expect(screen.getByText("Profissional")).toBeInTheDocument();
    });
});
