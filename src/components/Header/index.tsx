import { Container, Title, Menu } from "./styles";

export default function Header() {
    return(
        <Container>
            <Title href="/">Lacrei</Title>
            <Menu>
                <ul>
                    <a className={window.location.pathname === '/' ? 'active' : ''} href="/"><li>Home</li></a>
                    <a className={window.location.pathname === '/user' ? 'active' : ''} href="/user"><li>Pessoa Usuária</li></a>
                    <a className={window.location.pathname === '/profissional' ? 'active' : ''} href="/profissional"><li>Profissional</li></a>
                </ul>
            </Menu>
        </Container>
    );
}