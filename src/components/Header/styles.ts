import styled from 'styled-components';

export const Container = styled.header `
    height: 60px;
    background: ${({ theme }) => theme.colors.secondary};
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0px ${({ theme }) => theme.spacing.large}px;

    @media screen and (max-width: 1040px) {
        padding: 0px ${({ theme }) => theme.spacing.small}px;
    }

    @media screen and (max-width: 520px) {
        height: 78px;
        flex-direction: column;
        justify-content: center;
    }
`

export const Title = styled.a `
    font-size: ${({ theme }) => theme.fonts_size[3]}px;
    font-weight: 700;
    color: ${({ theme }) => theme.colors.green};
    text-decoration: none;
    transition: all ease .2s;

    &:hover {
        color: ${({ theme }) => theme.colors.green_hover};
    }
`

export const Menu = styled.nav `
    ul {
        display: flex;
        align-items: center;
    }

    ul a {
        text-decoration: none;
    }

    ul a li {
        color: ${({ theme }) => theme.colors.primary_font};
        list-style: none;
        transition: all ease .2s;
        font-size: ${({ theme }) => theme.fonts_size[0]}px;
        font-weight: 700;
    }

    ul a:hover li {
        color: ${({ theme }) => theme.colors.secondary_font};
    }

    ul a + a {
        margin-left: 40px;
    }

    .active li {
        color: ${({ theme }) => theme.colors.green};
    }

    .active:hover li {
        color: ${({ theme }) => theme.colors.green_hover};
    }

    @media screen and (max-width: 520px) {
        ul a li {
            font-size: ${({ theme }) => theme.fonts_size[5]}px;
        }

        ul a + a {
            margin-left: 20px;
        }
    }
`