import styled from "styled-components";

export const Container = styled.footer `
    margin: 0px ${({ theme }) => theme.spacing.large}px 0px ${({ theme }) => theme.spacing.large}px;
    padding-top: ${({ theme }) => theme.spacing.small}px;
    border-top: 1px solid ${({ theme }) => theme.colors.light_green};

    span {
        display: inline-block;
        font-size: 12px;
        margin-bottom: 8px;
    }

    @media screen and (max-width: 1040px) {
        margin: 56px ${({ theme }) => theme.spacing.small}px 0px ${({ theme }) => theme.spacing.small}px;
    }

    @media screen and (max-width: 520px) {
        padding-top: ${({ theme }) => theme.spacing.very_small}px;
        margin-top: 0px;
    }
`

export const Menu = styled.nav `
    ul {
        display: flex;
        align-items: center;
    }

    ul a {
        text-decoration: none;
    }

    ul a li {
        color: ${({ theme }) => theme.colors.primary_font};
        list-style: none;
        transition: all ease .2s;
        font-size: ${({ theme }) => theme.fonts_size[0]}px;
        font-weight: 400;
    }

    ul a:hover li {
        color: ${({ theme }) => theme.colors.secondary_font};
    }

    ul a + a {
        margin-left: 40px;
    }

    .active li {
        font-weight: 700;
    }

    .active:hover li {
        color: ${({ theme }) => theme.colors.secondary_font};
    }

    @media screen and (max-width: 520px) {
        ul {
            flex-direction: column;
            align-items: flex-start;
            justify-content: center;
        }

        ul a {
            line-height: 1.5;
        }

        ul a + a {
            margin-left: 0px;
        }

        ul a li {
            font-size: ${({ theme }) => theme.fonts_size[5]}px;
        }
    }
`

export const Links = styled.div `
    display: flex;
    justify-content: left;
    align-items: center;
    margin: ${({ theme }) => theme.spacing.big_small}px 0px;

    a {
        display: inline-block;
        height: 38px;
        font-size: ${({ theme }) => theme.fonts_size[3]}px;
        color: ${({ theme }) => theme.colors.green};
        transition: all ease .2s;
    }

    a:hover {
        color: ${({ theme }) => theme.colors.green_hover};
    }

    a + a {
        margin-left: 41px;
    }

    @media screen and (max-width: 520px) {
        margin: 10px 0px;
    }
`
