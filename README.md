# Desafio Lacrei Saúde

# Sobre o projeto

https://desafio-lacrei.netlify.app/

Site responsivo, feito com React, desenvolvido para o teste técnico de desenvolvedor front-end voluntário da Lacrei Saúde.

## Layout
<img src="src/assets/img/home.png"  alt="Tela 1 - Página inicial" title="Página inicial"/>

<img src="src/assets/img/user.png"  alt="Tela 2 - Pessoa Usuária" title="Página para usuário"/>

<img src="src/assets/img/profissional.png"  alt="Tela 3 - Profissional" title="Página para profissional"/>


# Tecnologias utilizadas
## Front end
- React
- HTML
- CSS
- TypeScript
- Styled Components
- React Router DOM
- Jest
- React Testing Library

## Implantação em produção
- Netlify

# Como executar o projeto

Pré-requisitos: npm / yarn  e NodeJs

```bash
# clonar repositório
git clone https://gitlab.com/YannFigueiredo/desafio-lacrei.git
# entrar na pasta do projeto
cd desafio-lacrei
# instalar dependências
npm install
# executar o projeto
npm start
```

# Autor

Yann Fabricio Cardoso de Figueiredo

https://www.linkedin.com/in/yann-figueiredo-5a5046102/

https://github.com/YannFigueiredo

